﻿Imports System
Imports HoracioCavalcanti.Xamurai.Concurrent
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()>
Public NotInheritable Class ThreadLocalRandomTests
    Private Shared ReadOnly Seed As Integer = Environment.TickCount

    <TestMethod()>
    Public Sub TestCollisionInNumberGeneration()
        Dim random As New Random(Seed)

        Dim safeRandom As New ThreadLocalRandom()

        For i = 0 To 999999
            Dim [next] As Integer = random.Next()
            Dim safeNext As Integer = safeRandom.Next()

            Assert.AreNotEqual([next], safeNext)
        Next
    End Sub
End Class