﻿Imports System.Reflection
Imports System.Resources
Imports System.Runtime.InteropServices

<Assembly: AssemblyTitle("Xamurai.Tests")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyProduct("Xamurai.Tests")>
<Assembly: AssemblyCopyright("Copyright © 2014 Horácio José Cavalcanti Filho (HoracioCavalcanti)")>
<Assembly: NeutralResourcesLanguage("en")>
<Assembly: AssemblyVersion("1.0.0")>
<Assembly: AssemblyFileVersion("1.0.0")>
<Assembly: Guid("e637e3bb-06db-479b-94e3-7f650113dae5")>
<Assembly: ComVisible(False)>