﻿Imports HoracioCavalcanti.Xamurai.Concurrent.Atomic
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Threading.Tasks

<TestClass()>
Public NotInheritable Class AtomicIntegerTests
    <TestMethod()>
    Public Sub TestConversion()
        Dim [integer] As Integer = New AtomicInteger(10)
        Dim atomicInteger As AtomicInteger = 20

        Assert.AreEqual([integer], 10)
        Assert.AreEqual(atomicInteger.Get(), 20)
    End Sub

    <TestMethod()>
    Public Sub TestGetAndAdd()
        Dim [integer] As Integer = 0
        Dim atomicInteger As New AtomicInteger()

        Const [to] As Integer = 10000

        For i As Integer = 0 To [to] - 1
            [integer] += i
        Next

        Parallel.For(0, [to], Sub(i)
                                  atomicInteger.GetAndAdd(i)
                              End Sub)

        Assert.AreEqual([integer], atomicInteger.Get())
    End Sub

    <TestMethod()>
    Public Sub TestGetAndDecrement()
        Dim [integer] As Integer = 10000
        Dim atomicInteger As New AtomicInteger(10000)

        Const [to] As Integer = 10000

        For i = 0 To [to] - 1
            [integer] -= 1
        Next

        Parallel.For(0, [to], Sub(i)
                                  atomicInteger.GetAndDecrement()
                              End Sub)

        Assert.AreEqual([integer], atomicInteger.Get())
    End Sub

    <TestMethod()>
    Public Sub TestGetAndIncrement()
        Dim [integer] As Integer = 0
        Dim atomicInteger As New AtomicInteger()

        Const [to] As Integer = 10000

        For i = 0 To [to] - 1
            [integer] += 1
        Next

        Parallel.For(0, [to], Sub(i)
                                  atomicInteger.GetAndIncrement()
                              End Sub)

        Assert.AreEqual([integer], atomicInteger.Get())
    End Sub

    <TestMethod()>
    Public Sub TestGetAndTestSet()
        Dim flag As New AtomicInteger()

        Dim t As Task = Task.Run(Sub()
                                     Dim toggle As Boolean = False

                                     While flag.Get() = 0
                                         toggle = Not toggle
                                     End While
                                 End Sub)

        Task.Delay(500).Wait()

        flag.Set(1)

        t.Wait()
    End Sub
End Class