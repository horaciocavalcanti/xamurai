﻿Imports System
Imports HoracioCavalcanti.Xamurai.Concurrent.Atomic
Imports System.Threading

Namespace Concurrent
    Public NotInheritable Class ThreadLocalRandom
        Inherits Random

        Private Shared ReadOnly DefaultSeed As AtomicInteger = New AtomicInteger(Environment.TickCount)
        Private ReadOnly _random As ThreadLocal(Of Random)

        Public Sub New()
            Me.New(DefaultSeed.IncrementAndGet())
        End Sub

        Public Sub New(ByVal seed As Integer)
            _random = New ThreadLocal(Of Random)(Function()
                                                     Return New Random(seed)
                                                 End Function)
        End Sub

        Private ReadOnly Property Current() As Random
            Get
                Return _random.Value
            End Get
        End Property

        Public Overrides Function [Next]() As Integer
            Return Current.[Next]()
        End Function

        Public Overrides Function [Next](ByVal maxValue As Integer) As Integer
            Return Current.[Next](maxValue)
        End Function

        Public Overrides Function [Next](ByVal minValue As Integer, ByVal maxValue As Integer) As Integer
            Return Current.[Next](minValue, maxValue)
        End Function

        Public Overrides Sub NextBytes(ByVal buffer As Byte())
            Current.NextBytes(buffer)
        End Sub

        Public Overrides Function NextDouble() As Double
            Return Current.NextDouble()
        End Function

        Protected Overrides Function Sample() As Double
            Throw New NotSupportedException()
        End Function
    End Class
End Namespace