﻿Imports System.Threading

Namespace Concurrent.Atomic
    ''' <summary>
    ''' An <c>int</c> value that may be updated atomically.
    ''' </summary>
    Public NotInheritable Class AtomicInteger
        Private _value As Integer

        ''' <summary>
        ''' Creates a new <see cref="HoracioCavalcanti.Xamurai.Concurrent.Atomic.AtomicInteger" /> with the given initial value.
        ''' </summary>
        ''' <param name="value">The initial value.</param>
        Public Sub New(Optional ByVal value As Integer = 0)
            _value = value
        End Sub

        ''' <summary>
        ''' Atomically sets the value to the given updated value if the current value <c>==</c> the expected value.
        ''' </summary>
        ''' <param name="expect">The expected value.</param>
        ''' <param name="update">The new value.</param>
        ''' <returns>True if successful. False return indicates that the actual value was not equal to the expected value.</returns>
        Public Function CompareAndSet(ByVal expect As Integer, ByVal update As Integer) As Boolean
            Return Interlocked.CompareExchange(_value, update, expect) = expect
        End Function

        ''' <summary>
        ''' Gets the current value.
        ''' </summary>
        ''' <returns>The current value.</returns>
        Public Function [Get]() As Integer
            Return Volatile.Read(_value)
        End Function

        ''' <summary>
        ''' Atomically adds the given value to the current value.
        ''' </summary>
        ''' <param name="delta">The value to add.</param>
        ''' <returns>The previous value.</returns>
        Public Function GetAndAdd(ByVal delta As Integer) As Integer
            Do
                Dim current As Integer = [Get]()
                Dim [next] As Integer = current + delta

                If CompareAndSet(current, [next]) Then
                    Return current
                End If
            Loop
        End Function

        ''' <summary>
        ''' Atomically decrements by one the current value.
        ''' </summary>
        ''' <returns>The previous value.</returns>
        Public Function GetAndDecrement() As Integer
            Do
                Dim current As Integer = [Get]()
                Dim [next] As Integer = current - 1

                If CompareAndSet(current, [next]) Then
                    Return current
                End If
            Loop
        End Function

        ''' <summary>
        ''' Atomically increments by one the current value.
        ''' </summary>
        ''' <returns>The previous value.</returns>
        Public Function GetAndIncrement() As Integer
            Do
                Dim current As Integer = [Get]()
                Dim [next] As Integer = current + 1

                If CompareAndSet(current, [next]) Then
                    Return current
                End If
            Loop
        End Function

        ''' <summary>
        ''' Atomically sets to the given value and returns the old value.
        ''' </summary>
        ''' <param name="newValue">The new value.</param>
        ''' <returns>The previous value.</returns>
        Public Function GetAndSet(ByVal newValue As Integer) As Integer
            Do
                Dim current As Integer = [Get]()

                If CompareAndSet(current, newValue) Then
                    Return current
                End If
            Loop
        End Function

        ''' <summary>
        ''' Atomically increments by one the current value.
        ''' </summary>
        ''' <returns>The updated value.</returns>
        Public Function IncrementAndGet() As Integer
            Do
                Dim current As Integer = [Get]()
                Dim [next] As Integer = current + 1

                If CompareAndSet(current, [next]) Then
                    Return [next]
                End If
            Loop
        End Function

        ''' <summary>
        ''' Sets to the given value.
        ''' </summary>
        ''' <param name="value">The new value.</param>
        Public Sub [Set](ByVal value As Integer)
            Volatile.Write(_value, value)
        End Sub

        Public Shared Widening Operator CType(ByVal atomicInteger As AtomicInteger) As Integer
            Return atomicInteger.Get()
        End Operator

        Public Shared Widening Operator CType(ByVal [integer] As Integer) As AtomicInteger
            Return New AtomicInteger([integer])
        End Operator
    End Class
End Namespace