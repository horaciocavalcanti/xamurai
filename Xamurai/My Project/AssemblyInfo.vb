﻿Imports System.Reflection
Imports System.Resources

<Assembly: AssemblyTitle("Xamurai")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyProduct("Xamurai")>
<Assembly: AssemblyCopyright("Copyright © 2014 Horácio José Cavalcanti Filho (HoracioCavalcanti)")>
<Assembly: NeutralResourcesLanguage("en")>
<Assembly: AssemblyVersion("1.0.0")>
<Assembly: AssemblyFileVersion("1.0.0")>